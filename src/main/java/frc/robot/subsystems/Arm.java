// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ArmConstants;

public class Arm extends SubsystemBase {
  /** Creates a new Arm. */
  private TalonFX arm;

  public Arm() {
    arm = new TalonFX(ArmConstants.ARM);

    arm.configFactoryDefault();
  }

  public void move(boolean isPressed) {
    if(isPressed) {
      arm.set(ControlMode.PercentOutput, 1.0);
    } else {
      arm.set(ControlMode.PercentOutput, 0.0);
    }
  }
}
